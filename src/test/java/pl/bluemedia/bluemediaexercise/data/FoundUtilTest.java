package pl.bluemedia.bluemediaexercise.data;

import static org.fest.assertions.Assertions.assertThat;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.google.common.collect.Multimap;

/**
 * Test class for {@link FoundFilter}
 * 
 * @author Jacek
 *
 */
public class FoundUtilTest {

  @Test
  public void testTransformFoundsToFoundsType() {
    // one found type should be only returned
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH));
    assertThat(FoundUtil.transformFoundsToFoundsType(userFounds)).containsOnly(FoundType.POLISH);

    // one found type should be only returned
    userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Polski 2", FoundType.POLISH),
          new Found(3L, "Fundusz Polski 3", FoundType.POLISH));
    assertThat(FoundUtil.transformFoundsToFoundsType(userFounds)).containsOnly(FoundType.POLISH);

    // three different found types should be returned
    userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Zagraniczny 1", FoundType.FOREIGN),
          new Found(3L, "Fundusz Pieniężny 1", FoundType.MONETARY));
    assertThat(FoundUtil.transformFoundsToFoundsType(userFounds)).hasSize(3).contains(FoundType.POLISH, FoundType.FOREIGN, FoundType.MONETARY);
  }

  @Test
  public void testTransformFoundsToMultimap() {
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Polski 2", FoundType.POLISH),
          new Found(3L, "Fundusz Polski 3", FoundType.POLISH), new Found(4L, "Fundusz Zagraniczny 1", FoundType.FOREIGN));
    // founds will be grouped by their types
    final Multimap<FoundType, Found> map = FoundUtil.transformFoundsToMultimap(userFounds);
    assertThat(map.get(FoundType.POLISH)).contains(new Found(1L, "Fundusz Polski 1", FoundType.POLISH),
          new Found(2L, "Fundusz Polski 2", FoundType.POLISH), new Found(3L, "Fundusz Polski 3", FoundType.POLISH));
    assertThat(map.get(FoundType.FOREIGN)).containsOnly(new Found(4L, "Fundusz Zagraniczny 1", FoundType.FOREIGN));
  }

}
