package pl.bluemedia.bluemediaexercise.engine;

import static org.fest.assertions.Assertions.assertThat;
import java.util.Arrays;
import java.util.List;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.bluemedia.bluemediaexercise.data.Found;
import pl.bluemedia.bluemediaexercise.data.FoundType;
import pl.bluemedia.bluemediaexercise.data.InvestmentStyle;

/**
 * Test class for {@link InvestmentValidator}
 * 
 * @author Jacek
 *
 */
public class InvestmentValidatorTest {

  @Rule
  public ExpectedException exception = ExpectedException.none();

  @Test
  public void testValidateMoney_nullValue() {
    exception.expect(NullPointerException.class);
    exception.expectMessage("You have to provide some money to invest!");
    InvestmentValidator.validateMoney(null);
  }

  @Test
  public void testValidateMoney_isNegativeValue() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("You have to provide value more than 0!");
    InvestmentValidator.validateMoney(BigMoney.of(CurrencyUnit.USD, -35d));
  }

  @Test
  public void testValidateFounds_nullValue() {
    exception.expect(NullPointerException.class);
    exception.expectMessage("You have to provide founds!");
    InvestmentValidator.validateFounds(null, InvestmentStyle.AGGRESSIVE);
  }

  @Test
  public void testValidateFounds_emptyList() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("List with founds cannot be empty!");
    InvestmentValidator.validateFounds(Arrays.asList(), InvestmentStyle.AGGRESSIVE);
  }

  @Test
  public void testValidateFounds_wrongSelectedFounds() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("You selected wrong founds for the investment style!");
    InvestmentValidator.validateFounds(Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH)), InvestmentStyle.AGGRESSIVE);
  }

  @Test
  public void testValidateInvestmentStyle_nullValue() {
    exception.expect(NullPointerException.class);
    exception.expectMessage("You have to provide investment style!");
    InvestmentValidator.validateInvestmentStyle(null);
  }

  @Test
  public void testContainsCorrectFounds() {
    // all necessary founds are provided = > true
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH),
          new Found(2L, "Fundusz Zagraniczny 1", FoundType.FOREIGN), new Found(3L, "Fundusz Pieniężny 1", FoundType.MONETARY));
    assertThat(InvestmentValidator.containsCorrectFounds(userFounds, InvestmentStyle.SAFE)).isTrue();

    // more founds then necessary => true
    userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Zagraniczny 1", FoundType.FOREIGN),
          new Found(3L, "Fundusz Pieniężny 1", FoundType.MONETARY), new Found(4L, "Fundusz Polski 2", FoundType.POLISH));
    assertThat(InvestmentValidator.containsCorrectFounds(userFounds, InvestmentStyle.SAFE)).isTrue();

    // missing at least one foreign found => false
    userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(3L, "Fundusz Pieniężny 1", FoundType.MONETARY));
    assertThat(InvestmentValidator.containsCorrectFounds(userFounds, InvestmentStyle.SAFE)).isFalse();
  }

}
