package pl.bluemedia.bluemediaexercise.engine;

import static org.fest.assertions.Assertions.assertThat;
import java.util.Arrays;
import java.util.List;
import org.joda.money.BigMoney;
import org.joda.money.CurrencyUnit;
import org.junit.Test;
import pl.bluemedia.bluemediaexercise.data.Found;
import pl.bluemedia.bluemediaexercise.data.FoundType;
import pl.bluemedia.bluemediaexercise.data.InvestmentResultsDto;
import pl.bluemedia.bluemediaexercise.data.InvestmentStyle;

/**
 * Test class for {@link InvestmentCalculator}
 * 
 * @author Jacek
 *
 */
public class InvestmentCalculatorTest {

  private InvestmentCalculator underTest = new InvestmentCalculator();

  @Test
  public void testSplitTheMoney_example1() {
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Polski 2", FoundType.POLISH),
          new Found(3L, "Fundusz Zagraniczny 1", FoundType.FOREIGN), new Found(4L, "Fundusz Zagraniczny 2", FoundType.FOREIGN),
          new Found(5L, "Fundusz Zagraniczny 3", FoundType.FOREIGN), new Found(6L, "Fundusz Pieniężny 1", FoundType.MONETARY));

    final InvestmentResultsDto investmentResult = underTest.calculate(BigMoney.of(CurrencyUnit.USD, 10000), InvestmentStyle.SAFE, userFounds);

    assertThat(investmentResult.getInvestments().get(0)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(1L, FoundType.POLISH, "Fundusz Polski 1", BigMoney.of(CurrencyUnit.USD, 1000), 10d));
    assertThat(investmentResult.getInvestments().get(1)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(2L, FoundType.POLISH, "Fundusz Polski 2", BigMoney.of(CurrencyUnit.USD, 1000), 10d));
    assertThat(investmentResult.getInvestments().get(2)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(3L, FoundType.FOREIGN, "Fundusz Zagraniczny 1", BigMoney.of(CurrencyUnit.USD, 2500), 25d));
    assertThat(investmentResult.getInvestments().get(3)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(4L, FoundType.FOREIGN, "Fundusz Zagraniczny 2", BigMoney.of(CurrencyUnit.USD, 2500), 25d));
    assertThat(investmentResult.getInvestments().get(4)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(5L, FoundType.FOREIGN, "Fundusz Zagraniczny 3", BigMoney.of(CurrencyUnit.USD, 2500), 25d));
    assertThat(investmentResult.getInvestments().get(5)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(6L, FoundType.MONETARY, "Fundusz Pieniężny 1", BigMoney.of(CurrencyUnit.USD, 500), 5d));

    assertThat(investmentResult.getUnassignedMoney()).isEqualTo(BigMoney.zero(CurrencyUnit.USD));
  }

  @Test
  public void testSplitTheMoney_example2() {
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Polski 2", FoundType.POLISH),
          new Found(3L, "Fundusz Zagraniczny 1", FoundType.FOREIGN), new Found(4L, "Fundusz Zagraniczny 2", FoundType.FOREIGN),
          new Found(5L, "Fundusz Zagraniczny 3", FoundType.FOREIGN), new Found(6L, "Fundusz Pieniężny 1", FoundType.MONETARY));

    final InvestmentResultsDto investmentResult = underTest.calculate(BigMoney.of(CurrencyUnit.USD, 10001), InvestmentStyle.SAFE, userFounds);

    assertThat(investmentResult.getInvestments().get(0)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(1L, FoundType.POLISH, "Fundusz Polski 1", BigMoney.of(CurrencyUnit.USD, 1000), 9.99d));
    assertThat(investmentResult.getInvestments().get(1)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(2L, FoundType.POLISH, "Fundusz Polski 2", BigMoney.of(CurrencyUnit.USD, 1000), 9.99d));
    assertThat(investmentResult.getInvestments().get(2)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(3L, FoundType.FOREIGN, "Fundusz Zagraniczny 1", BigMoney.of(CurrencyUnit.USD, 2500), 24.99d));
    assertThat(investmentResult.getInvestments().get(3)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(4L, FoundType.FOREIGN, "Fundusz Zagraniczny 2", BigMoney.of(CurrencyUnit.USD, 2500), 24.99d));
    assertThat(investmentResult.getInvestments().get(4)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(5L, FoundType.FOREIGN, "Fundusz Zagraniczny 3", BigMoney.of(CurrencyUnit.USD, 2500), 24.99d));
    assertThat(investmentResult.getInvestments().get(5)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(6L, FoundType.MONETARY, "Fundusz Pieniężny 1", BigMoney.of(CurrencyUnit.USD, 500), 4.99d));

    assertThat(investmentResult.getUnassignedMoney()).isEqualTo(BigMoney.of(CurrencyUnit.USD, 1));
  }

  @Test
  public void testSplitTheMoney_example3() {
    List<Found> userFounds = Arrays.asList(new Found(1L, "Fundusz Polski 1", FoundType.POLISH), new Found(2L, "Fundusz Polski 2", FoundType.POLISH),
          new Found(3L, "Fundusz Polski 3", FoundType.POLISH), new Found(4L, "Fundusz Zagraniczny 1", FoundType.FOREIGN),
          new Found(5L, "Fundusz Zagraniczny 2", FoundType.FOREIGN), new Found(6L, "Fundusz Pieniężny 1", FoundType.MONETARY));

    final InvestmentResultsDto investmentResult = underTest.calculate(BigMoney.of(CurrencyUnit.USD, 10000), InvestmentStyle.SAFE, userFounds);

    assertThat(investmentResult.getInvestments().get(0)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(1L, FoundType.POLISH, "Fundusz Polski 1", BigMoney.of(CurrencyUnit.USD, 666), 6.66d));
    assertThat(investmentResult.getInvestments().get(1)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(2L, FoundType.POLISH, "Fundusz Polski 2", BigMoney.of(CurrencyUnit.USD, 666), 6.66d));
    assertThat(investmentResult.getInvestments().get(2)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(3L, FoundType.POLISH, "Fundusz Polski 3", BigMoney.of(CurrencyUnit.USD, 666), 6.66d));
    assertThat(investmentResult.getInvestments().get(3)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(4L, FoundType.FOREIGN, "Fundusz Zagraniczny 1", BigMoney.of(CurrencyUnit.USD, 3750), 37.5d));
    assertThat(investmentResult.getInvestments().get(4)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(5L, FoundType.FOREIGN, "Fundusz Zagraniczny 2", BigMoney.of(CurrencyUnit.USD, 3750), 37.5d));
    assertThat(investmentResult.getInvestments().get(5)).isEqualTo(
          new InvestmentResultsDto().new InvestmentDto(6L, FoundType.MONETARY, "Fundusz Pieniężny 1", BigMoney.of(CurrencyUnit.USD, 500), 5d));

    assertThat(investmentResult.getUnassignedMoney()).isEqualTo(BigMoney.of(CurrencyUnit.USD, 2));
  }



}
