package pl.bluemedia.bluemediaexercise.data;

import java.util.List;
import org.joda.money.BigMoney;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Represents all needed information after a calculation.
 * 
 * @author Jacek
 *
 */
public class InvestmentResultsDto {

  private List<InvestmentDto> investments;
  private BigMoney unassignedMoney;

  public List<InvestmentDto> getInvestments() {
    return investments;
  }

  public void setInvestments(List<InvestmentDto> investments) {
    this.investments = investments;
  }

  public BigMoney getUnassignedMoney() {
    return unassignedMoney;
  }

  public void setUnassignedMoney(BigMoney unassignedMoney) {
    this.unassignedMoney = unassignedMoney;
  }

  public class InvestmentDto {

    private Long id;
    private FoundType foundType;
    private String name;
    private BigMoney amount;
    private double percentage;

    public InvestmentDto(Long id, FoundType foundType, String name, BigMoney amount, double percentage) {
      this.id = id;
      this.foundType = foundType;
      this.name = name;
      this.amount = amount;
      this.percentage = percentage;
    }

    public Long getId() {
      return id;
    }

    public FoundType getFoundType() {
      return foundType;
    }

    public String getName() {
      return name;
    }

    public BigMoney getAmount() {
      return amount;
    }

    public double getPercentage() {
      return percentage;
    }

    @Override
    public boolean equals(Object obj) {
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      final InvestmentDto other = (InvestmentDto) obj;
      //@formatter:off
      return Objects.equal(this.id, other.id) && 
            Objects.equal(this.foundType, other.foundType) &&
            Objects.equal(this.name, other.name) &&
            Objects.equal(this.amount, other.amount) &&
            Objects.equal(this.percentage, other.percentage);
      //@formatter:on
    }

    @Override
    public int hashCode() {
      return Objects.hashCode(this.id, this.foundType, this.name, this.amount, this.percentage);
    }

    @Override
    public String toString() {
      return MoreObjects.toStringHelper(this).add("id", id).add("found type", foundType).add("name", name).add("amount", amount.toString())
            .add("percentage", percentage).toString();
    }

  }
}
