package pl.bluemedia.bluemediaexercise.data;

import static pl.bluemedia.bluemediaexercise.data.FoundType.FOREIGN;
import static pl.bluemedia.bluemediaexercise.data.FoundType.MONETARY;
import static pl.bluemedia.bluemediaexercise.data.FoundType.POLISH;
import java.math.BigDecimal;
import java.util.Map;
import com.google.common.collect.ImmutableMap;

/**
 * Available investment styles in the system.
 * 
 * @author Jacek
 *
 */
public enum InvestmentStyle {

  SAFE(ImmutableMap.<FoundType, BigDecimal>builder().put(POLISH, BigDecimal.valueOf(0.2)).put(FOREIGN, BigDecimal.valueOf(0.75))
        .put(MONETARY, BigDecimal.valueOf(0.05)).build()),

  BALANCED(ImmutableMap.<FoundType, BigDecimal>builder().put(POLISH, BigDecimal.valueOf(0.3)).put(FOREIGN, BigDecimal.valueOf(0.6))
        .put(MONETARY, BigDecimal.valueOf(0.1)).build()),

  AGGRESSIVE(ImmutableMap.<FoundType, BigDecimal>builder().put(POLISH, BigDecimal.valueOf(0.4)).put(FOREIGN, BigDecimal.valueOf(0.2))
        .put(MONETARY, BigDecimal.valueOf(0.4)).build());

  private final Map<FoundType, BigDecimal> style;

  private InvestmentStyle(final Map<FoundType, BigDecimal> style) {
    this.style = style;
  }

  public Map<FoundType, BigDecimal> getStyle() {
    return style;
  }

}
