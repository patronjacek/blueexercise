package pl.bluemedia.bluemediaexercise.data;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Represents a found, which can be selected by user to invest.
 * 
 * @author Jacek
 *
 */
public class Found {

  private Long id;
  private String name;
  private FoundType foundType;

  public Found(final Long id, final String name, final FoundType foundType) {
    this.id = id;
    this.name = name;
    this.foundType = foundType;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public FoundType getFoundType() {
    return foundType;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final Found other = (Found) obj;
    //@formatter:off
    return Objects.equal(this.id, other.id) && 
              Objects.equal(this.name, other.name) && 
              Objects.equal(this.foundType, other.foundType);
    //@formatter:on
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.id, this.name, this.foundType);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).add("id", id).add("name", name).add("found type", foundType).toString();
  }

}
