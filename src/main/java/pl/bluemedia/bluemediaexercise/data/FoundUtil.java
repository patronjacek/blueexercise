package pl.bluemedia.bluemediaexercise.data;

import java.util.List;
import java.util.Set;
import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Collections2;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

/**
 * Helper class for transforming lists of the founds.
 * 
 * @author Jacek
 *
 */
public class FoundUtil {

  public static Set<FoundType> transformFoundsToFoundsType(final List<Found> founds) {
    return Sets.newHashSet(Collections2.transform(founds, foundToFoundTypeFunc));
  }

  public static Multimap<FoundType, Found> transformFoundsToMultimap(final List<Found> founds) {
    final Multimap<FoundType, Found> map = ArrayListMultimap.create();
    for (final Found found : founds) {
      map.put(found.getFoundType(), found);
    }
    return map;
  }

  private static Function<Found, FoundType> foundToFoundTypeFunc = new Function<Found, FoundType>() {
    @Override
    public FoundType apply(Found found) {
      return found.getFoundType();
    }
  };

}
