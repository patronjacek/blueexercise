package pl.bluemedia.bluemediaexercise.data;

/**
 * Available types of the founds in the system.
 * 
 * @author Jacek
 *
 */
public enum FoundType {

  POLISH, FOREIGN, MONETARY;

}
