package pl.bluemedia.bluemediaexercise.engine;

import static pl.bluemedia.bluemediaexercise.engine.InvestmentValidator.validateFounds;
import static pl.bluemedia.bluemediaexercise.engine.InvestmentValidator.validateInvestmentStyle;
import static pl.bluemedia.bluemediaexercise.engine.InvestmentValidator.validateMoney;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import org.joda.money.BigMoney;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import pl.bluemedia.bluemediaexercise.data.Found;
import pl.bluemedia.bluemediaexercise.data.FoundType;
import pl.bluemedia.bluemediaexercise.data.FoundUtil;
import pl.bluemedia.bluemediaexercise.data.InvestmentResultsDto;
import pl.bluemedia.bluemediaexercise.data.InvestmentResultsDto.InvestmentDto;
import pl.bluemedia.bluemediaexercise.data.InvestmentStyle;

public final class InvestmentCalculator {

  /**
   * Core method of the system used for calculating how to split the money.
   * 
   * @param money - amount which user wants to invest.
   * @param investmentStyle - user's selected style for investment strategy.
   * @param founds - user's selected founds which he/she want to invest for.
   * 
   * @return {@link InvestmentResultsDto} which represents output of the calculation.
   */
  public InvestmentResultsDto calculate(final BigMoney money, final InvestmentStyle investmentStyle, final List<Found> founds) {
    // check if input data is correct
    validateMoney(money);
    validateFounds(founds, investmentStyle);
    validateInvestmentStyle(investmentStyle);

    // split the money and check unassigned money
    final Multimap<FoundType, Found> groupedFounds = FoundUtil.transformFoundsToMultimap(founds);
    final InvestmentResultsDto calculatedMoney = splitTheMoney(money.withScale(0), investmentStyle, groupedFounds);
    checkUnassignedMoney(money, calculatedMoney);

    // return correct results
    return calculatedMoney;
  }

  // @formatter:off
  private InvestmentResultsDto splitTheMoney(final BigMoney money, final InvestmentStyle investmentStyle, final Multimap<FoundType, Found> groupedFounds) {
    final InvestmentResultsDto investmentResult = new InvestmentResultsDto();
    final List<InvestmentDto> investments = Lists.newArrayList();
    for (final FoundType foundType : investmentStyle.getStyle().keySet()) {
      final BigMoney moneyRelevantForFoundType = money.multiplyRetainScale(investmentStyle.getStyle().get(foundType), RoundingMode.DOWN);
      for (final Found found : groupedFounds.get(foundType)) {
        final BigMoney moneyPerSingleFound = moneyRelevantForFoundType.dividedBy(groupedFounds.get(foundType).size(), RoundingMode.DOWN);
        final double percentageValuePerFound = (moneyPerSingleFound.getAmount().divide(money.getAmount(), 4, RoundingMode.DOWN)).multiply(BigDecimal.valueOf(100)).doubleValue();
        investments.add(investmentResult.new InvestmentDto(found.getId(), foundType, found.getName(), moneyPerSingleFound, percentageValuePerFound));
      }
    }
    investmentResult.setInvestments(investments);
    return investmentResult;
  }
  //@formatter:on

  private void checkUnassignedMoney(final BigMoney money, final InvestmentResultsDto calculatedMoney) {
    final List<InvestmentDto> investments = calculatedMoney.getInvestments();
    BigMoney investedMoney = BigMoney.zero(money.getCurrencyUnit());
    for (final InvestmentDto investment : investments) {
      investedMoney = investedMoney.plus(investment.getAmount());
    }
    calculatedMoney.setUnassignedMoney(money.minus(investedMoney));
  }

}
