package pl.bluemedia.bluemediaexercise.engine;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.List;
import java.util.Set;
import org.joda.money.BigMoney;
import pl.bluemedia.bluemediaexercise.data.Found;
import pl.bluemedia.bluemediaexercise.data.FoundType;
import pl.bluemedia.bluemediaexercise.data.FoundUtil;
import pl.bluemedia.bluemediaexercise.data.InvestmentStyle;

/**
 * Custom validations needed for calculating the money.
 * 
 * @author Jacek
 *
 */
final class InvestmentValidator {

  public static void validateMoney(final BigMoney money) throws IllegalArgumentException {
    checkNotNull(money, "You have to provide some money to invest!");
    checkArgument(money.isPositive(), "You have to provide value more than 0!");
  }

  public static void validateFounds(final List<Found> founds, final InvestmentStyle investmentStyle) {
    checkNotNull(founds, "You have to provide founds!");
    checkArgument(!founds.isEmpty(), "List with founds cannot be empty!");
    checkArgument(containsCorrectFounds(founds, investmentStyle), "You selected wrong founds for the investment style!");
  }

  public static void validateInvestmentStyle(final InvestmentStyle investmentStyle) {
    checkNotNull(investmentStyle, "You have to provide investment style!");
  }

  static boolean containsCorrectFounds(final List<Found> founds, final InvestmentStyle investmentStyle) {
    final Set<FoundType> requiredFoundTypesFromStyle = investmentStyle.getStyle().keySet();
    final Set<FoundType> userFoundTypes = FoundUtil.transformFoundsToFoundsType(founds);
    return requiredFoundTypesFromStyle.equals(userFoundTypes);
  }

}
